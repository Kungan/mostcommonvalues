﻿using Ookii.Dialogs.Wpf;

namespace MaxValue.Services
{
	internal class FileService
	{
		/// <summary>
		/// Диалог выбора папки. Так как среди встроенных в WPF диалогов,
		/// предназначенных именно для выбора папки, нет, использована библиотека.
		/// </summary>
		private readonly VistaFolderBrowserDialog folderBrowserDialog = new();

		/// <summary>
		/// Выбор папки.
		/// </summary>
		/// <returns>Путь к папке, либо <see langword="null"/>, если выбор был отменен.</returns>
		public string? SelectFolder()
		{
			if (folderBrowserDialog.ShowDialog() == true)
			{
				return folderBrowserDialog.SelectedPath;
			}

			return null;
		}
	}
}
