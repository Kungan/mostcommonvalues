﻿using System.Windows;

namespace MaxValue.Services
{
	/// <summary>
	/// Класс для отображения сообщений.
	/// </summary>
	internal class MessageService
	{
		/// <summary>
		/// Показывает ошибку.
		/// </summary>
		/// <param name="message">Текст ошибки.</param>
		public static void ShowError(string message)
		{
			MessageBox.Show(message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
		}
	}
}
