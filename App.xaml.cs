﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MaxValue.Model;
using MaxValue.ViewModel;
using MaxValue.ViewModel.Serializing;

namespace MaxValue
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			// Новые десериализаторы добавлять здесь.
			Dictionary<string, IDeserializer<IStringContainer>?> serializers = new()
			{
				{ ".xml", new XmlDeserializer<XmlValuesContainer>() },
				{ ".json", new JsonSerializer<JsonValuesContainer>() }
			};
			MainWindow window = new()
			{
				DataContext = new MainWindowVm(serializers)
			};
			window.Show();
		}
	}
}
