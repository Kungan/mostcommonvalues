﻿using System.Xml.Serialization;

namespace MaxValue.Model
{
	/// <summary>
	/// Тип, представляющий
	/// </summary>
	[XmlRoot(ElementName = "Root")]
	public class XmlValuesContainer: IStringContainer
	{
		[XmlArray("Values")]
		[XmlArrayItem("Value")]
		public string[]? Values { get; set; }

		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string? Xsi { get; set; }

		[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string? Xsd { get; set; }
	}
}
