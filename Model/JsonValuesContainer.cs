﻿namespace MaxValue.Model
{
	class JsonValuesContainer: IStringContainer
	{
		public string[]? Values { get; set; }
	}
}
