﻿namespace MaxValue.Model
{
	/// <summary>
	/// Интерфейс хранилища значений.
	/// </summary>
	public interface IValuesContainer<out T>
	{
		T[]? Values { get; }
	}
}