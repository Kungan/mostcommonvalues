﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MaxValue.Model
{
	class Statistics
	{
		/// <summary>
		/// Возвращает наиболее часто встречающиеся в списке объекты.
		/// </summary>
		/// <typeparam name="T">Тип объектов.</typeparam>
		/// <param name="values">Объекты.</param>
		/// <returns>Количество наиболее часто встречающихся объектов и сами объекты (может быть более одного
		/// если они встречаются одинаковое количество раз).</returns>
		public static (int count, IEnumerable<T> values) GetMostCommonValues<T>(IEnumerable<T> values)
		{

			IGrouping<int, IGrouping<T, T>>? mostCommonValuesGroups = values
				.GroupBy(value => value)
				.GroupBy(group => group.Count())
				.OrderByDescending(superGroup => superGroup.Key)
				.FirstOrDefault();
			IEnumerable<T>? mostCommonValues = mostCommonValuesGroups?.Select(superGroup => superGroup.Key);

			return (
				count: mostCommonValuesGroups?.Key ?? 0,
				values: mostCommonValues ?? Array.Empty<T>()
			);
		}
	}
}
