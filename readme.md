﻿Исходные данные:
В папке «Files» находятся заранее заготовленные файлы разных типов (xml и json), которые содержат некоторый набор данных. Структура файлов одного типа одинаковая. 
Требования:
1.	Считать значения элементов коллекции «Values» из всех файлов (предусмотреть возможность расширения новыми типами файлов).
2.	Реализовать алгоритм (использовать значения, полученные из всех файлов): найти значение, которое встречается наибольшее количество раз.
3.	Вывести найденное значение и количество повторений этого значения на экран.

Реализовать одним из двух вариантов:
1.	Asp Net Core, с выводом результата на веб страницу
2.	WPF Net Framework.
