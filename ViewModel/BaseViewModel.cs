﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MaxValue.ViewModel
{
	/// <summary>
	/// Базовый класс модели представления.
	/// </summary>
	internal abstract class BaseViewModel: INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Поднимает событие изменения свойства.
		/// </summary>
		/// <param name="propertyName">Имя свойства. По умолчанию - того, которое вызывает метод.</param>
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}

}
