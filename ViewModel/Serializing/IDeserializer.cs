﻿namespace MaxValue.ViewModel.Serializing
{
	/// <summary>
	/// Интерфейс десериализатора.
	/// </summary>
	/// <typeparam name="T">Тип объекта, который десериализуется.</typeparam>
	internal interface IDeserializer<out T>
	{
		/// <summary>
		/// Десериализует файл.
		/// </summary>
		/// <param name="uri">Путь к файлу.</param>
		/// <returns>Десериализованный объект либо <see langword="null"/> в случае неудачи.</returns>
		T? Deserialize(string uri);
	}
}