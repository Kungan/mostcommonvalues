﻿using System;

namespace MaxValue.ViewModel.Serializing
{
	/// <summary>
	/// Исключение, выбрасываемое при ошибке десериализации.
	/// </summary>
	[Serializable]
	internal class SerializerException : Exception
	{
		public SerializerException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}