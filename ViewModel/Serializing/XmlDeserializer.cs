﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace MaxValue.ViewModel.Serializing
{
	/// <summary>
	/// Десериализатор XML-файлов.
	/// </summary>
	/// <typeparam name="T">Тип десериализуемого объекта.</typeparam>
	internal class XmlDeserializer<T> : IDeserializer<T>
	{
		public T? Deserialize(string uri)
		{
			XmlSerializer serializer = new(typeof(T));
			using XmlReader reader = XmlReader.Create(uri);
			try {
				return (T?)serializer.Deserialize(reader);
			}
			catch (Exception e) {
				throw new SerializerException($"Ошибка при десериализации файла {uri}.", e);
			}
		}
	}
}
