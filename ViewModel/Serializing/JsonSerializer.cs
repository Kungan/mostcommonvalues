﻿using System.IO;
using System.Text.Json;

namespace MaxValue.ViewModel.Serializing
{
	/// <summary>
	/// Десериализатор JSON-файлов.
	/// </summary>
	/// <typeparam name="T">Тип десериализуемого объекта.</typeparam>
	internal class JsonSerializer<T>: IDeserializer<T>
	{
		public T? Deserialize(string uri)
		{
			string jsonString = File.ReadAllText(uri);
			return JsonSerializer.Deserialize<T>(jsonString);
		}
	}
}
