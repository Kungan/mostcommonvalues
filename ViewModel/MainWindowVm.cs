﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using MaxValue.Model;
using MaxValue.Services;
using MaxValue.ViewModel.Serializing;

namespace MaxValue.ViewModel
{
	/// <summary>
	/// Модель представления окна приложения.
	/// </summary>
	internal class MainWindowVm: BaseViewModel
	{
		/// <summary>
		/// Десериализаторы.
		/// </summary>
		private readonly Dictionary<string, IDeserializer<IStringContainer>?> deserializers;

		/// <summary>
		///	Команда сохранения.
		/// </summary>
		public RelayCommand SelectFolder { get; }

		/// <summary>
		///	Команда выхода из приложения.
		/// </summary>
		public RelayCommand QuitCommand { get; }

		/// <summary>
		/// Количество одинаковых значений.
		/// </summary>
		public int ValuesCount
		{
			get => valuesCount;
			set {
				valuesCount = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// Сервис работы с файловой системой.
		/// </summary>
		private readonly FileService fileService = new();
		private int valuesCount;

		/// <summary>
		/// Наибольшие значение коллекции Values во всех открытых файлах.
		/// </summary>
		public ObservableCollection<string> MostCommonValues { get; } = new();

		/// <summary>
		/// Пустой конструктор для визуального редактора. Без него не удается задать DataContext времени выполнения.
		/// </summary>
		public MainWindowVm()
		{
			SelectFolder = new RelayCommand(_ => SelectFolder_Executed());
			QuitCommand = new RelayCommand(_ => QuitCommand_Executed());
		}

		public MainWindowVm(Dictionary<string, IDeserializer<IStringContainer>?> deserializers) : this()
		{
			this.deserializers = deserializers;
			SelectFolder = new RelayCommand(_ => SelectFolder_Executed());
			QuitCommand = new RelayCommand(_ => QuitCommand_Executed());
		}


		private void SelectFolder_Executed()
		{
			string? path = fileService.SelectFolder();
			if (path == null)
			{
				return;
			}

			try
			{
				UpdateStatistics(path);
			}
			catch (UnauthorizedAccessException e)
			{
				MessageService.ShowError($"При открытии файлов в папке {path} произошла ошибка: {e.Message}");
			}
			catch (SerializerException e)
			{
				MessageService.ShowError($"При десериализации файлов в папке {path} произошла ошибка: {e.Message}");
			}
		}

		/// <summary>
		/// Обновляет информацию о наиболее часто используемых значениях и их количестве.
		/// </summary>
		/// <param name="folderPath">Путь к папке с файлами, хранящими значения.</param>
		private void UpdateStatistics(string folderPath)
		{
			List<string> values = GetValuesInFolder(folderPath);
			(int count, IEnumerable<string> mostCommonValues) = Statistics.GetMostCommonValues(values);
			ValuesCount = count;
			MostCommonValues.Clear();
			foreach (string value in mostCommonValues)
			{
				MostCommonValues.Add(value);
			}
		}

		/// <summary>
		/// Получает значения, хранящиеся в файлах.
		/// </summary>
		/// <param name="folderPath">Путь к папке с файлами.</param>
		/// <returns>Значения.</returns>
		private List<string> GetValuesInFolder(string folderPath)
		{
			string[] files = Directory.GetFiles(folderPath);
			List<string> values = new();

			foreach (string file in files)
			{
				string extension = Path.GetExtension(file);

				if (deserializers.TryGetValue(extension, out IDeserializer<IStringContainer> serializer))
				{
					IStringContainer? container = serializer.Deserialize(file);
					values.AddRange(container?.Values ?? Array.Empty<string>());
				}
			}
			return values;
		}

		private static void QuitCommand_Executed()
		{
			Application.Current.Shutdown();
		}
	}
}
